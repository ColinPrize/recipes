from django.contrib import admin

from recipes.models import Recipe,RecipeStep,RecipeIngredients

@admin.register(Recipe)
class RecipeAdmin (admin.ModelAdmin):
    lest_display = (
        "title",
        'id',
    )
@admin.register(RecipeStep)
class RecipeStepAdmi(admin.ModelAdmin):
    lest_display =(
        "step_numer",
        "instruction",
        "id",
    )

@admin.register(RecipeIngredients)
class RecipeIngrpAdmi(admin.ModelAdmin):
    lest_display =(
        "amount",
        "food_item",
        "id",
    )
